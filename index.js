/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
		let fullName = prompt("Enter your fullname: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	printWelcomeMessage();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function showFaveBands(){
		let bandOne = "Linkin Park";
		let bandTwo = "Queen";
		let bandThree = "Michael Learns to Rock";
		let bandFour = "Green Day";
		let bandFive = "Coldplay";

		console.log("1. " + bandOne);
		console.log("2. " + bandTwo);
		console.log("3. " + bandThree);
		console.log("4. " + bandFour);
		console.log("5. " + bandFive);

	}

	showFaveBands();


/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function showFaveMovies(){
		let movieOne = "Cast Away";
		let movieOneRating = "89%";
		let movieTwo = "Green Mile";
		let movieTwoRating = "79%";
		let movieThree = "The Terminal";
		let movieThreeRating = "61%";
		let movieFour = "Saving Private Ryan";
		let movieFourRating = "94%";
		let movieFive = "Forest Gump";
		let movieFiveRating = "71%";

		console.log("1. " + movieOne);
		console.log("Rotten Tomatoes Rating: " + movieOneRating);
		console.log("2. " + movieTwo);
		console.log("Rotten Tomatoes Rating: " + movieTwoRating);
		console.log("3. " + movieThree);
		console.log("Rotten Tomatoes Rating: " + movieThreeRating);
		console.log("4. " + movieFour);
		console.log("Rotten Tomatoes Rating: " + movieFourRating);
		console.log("5. " + movieFive);
		console.log("Rotten Tomatoes Rating: " + movieFiveRating);

	}

	showFaveMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printFriends();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);